const mongoose = require('mongoose');
const express = require('express');
const app = express();
const port = 3000;

const chienRouter = require('./routes/chien');
const ecoleRouter = require('./routes/ecole');
const moniteurRouter = require('./routes/moniteur');
const proprietaireRouter = require('./routes/proprietaire');
const sessionRouter = require('./routes/session');

app.use(express.json());

main().catch(err => console.log(err));

async function main() {
    await mongoose.connect('mongodb://127.0.0.1:27017/db_chein');
    console.log("Connected to the database");

    app.get('/', async (req, res) => {
        res.send("Welcome to the REST API");
    });

    app.use('/chien', chienRouter);
    app.use('/ecole', ecoleRouter);
    app.use('/moniteur', moniteurRouter);
    app.use('/proprietaire', proprietaireRouter);
    app.use('/session', sessionRouter);

    await app.listen(port);
    console.log(`App listening on port ${port}`);
}
