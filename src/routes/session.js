const express = require('express');
const router = express.Router();
const Session = require('../models/sessionModel');

router.get('/', async (req, res) => {
    try {
        const sessions = await Session.find();
        res.send(sessions);
    } catch (error) {
        res.status(500).send({ message: error.message });
    }
});

router.get('/:id', async (req, res) => {
    const { id } = req.params;
    try {
        const session = await Session.findById(id);
        if (!session) {
            return res.status(404).send({ message: 'Session non trouvée' });
        }
        res.send(session);
    } catch (error) {
        res.status(500).send({ message: error.message });
    }
});

router.get('/aggregation', async (req, res) => {
    try {
        const aggregationResult = await Session.aggregate([
            {
                $group: {
                    _id: "$cour.niveau",
                    total: { $sum: {$gt : 3} }
                }
            }
        ]);
        res.send(aggregationResult);
    } catch (error) {
        res.status(500).send({ message: error.message });
    }
});

router.post('/', async (req, res) => {
    try {
        const session = await Session.create(req.body);
        res.status(201).send(session);
    } catch (error) {
        res.status(400).send({ message: error.message });
    }
});

router.put('/:id', async (req, res) => {
    const { id } = req.params;
    try {
        const sessionModifiee = await Session.findByIdAndUpdate(id, req.body, { new: true });
        if (!sessionModifiee) {
            return res.status(404).send({ message: 'Session non trouvée' });
        }
        res.send(sessionModifiee);
    } catch (error) {
        res.status(400).send({ message: error.message });
    }
});

router.delete('/:id', async (req, res) => {
    const { id } = req.params;
    try {
        const sessionSupprimee = await Session.findByIdAndDelete(id);
        if (!sessionSupprimee) {
            return res.status(404).send({ message: 'Session non trouvée' });
        }
        res.send(sessionSupprimee);
    } catch (error) {
        res.status(500).send({ message: error.message });
    }
});

module.exports = router;
