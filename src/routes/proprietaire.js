const express = require('express');
const router = express.Router();
const Proprietaire = require('../models/proprietaireModel');

router.get('/', async (req, res) => {
    try {
        const proprietaires = await Proprietaire.find();
        res.send(proprietaires);
    } catch (error) {
        res.status(500).send({ message: error.message });
    }
});

router.get('/:id', async (req, res) => {
    const { id } = req.params;
    try {
        const proprietaire = await Proprietaire.findById(id);
        if (!proprietaire) {
            return res.status(404).send({ message: 'Propriétaire non trouvé' });
        }
        res.send(proprietaire);
    } catch (error) {
        res.status(500).send({ message: error.message });
    }
});

router.get('/aggregation', async (req, res) => {
    try {
        const aggregationResult = await Proprietaire.aggregate([
            {
                $match: { origine: "Sion" }
            }
        ]);
        res.send(aggregationResult);
    } catch (error) {
        res.status(500).send({ message: error.message });
    }
});

router.post('/', async (req, res) => {
    try {
        const proprietaire = await Proprietaire.create(req.body);
        res.status(201).send(proprietaire);
    } catch (error) {
        res.status(400).send({ message: error.message });
    }
});

router.put('/:id', async (req, res) => {
    const { id } = req.params;
    try {
        const proprietaireModifie = await Proprietaire.findByIdAndUpdate(id, req.body, { new: true });
        if (!proprietaireModifie) {
            return res.status(404).send({ message: 'Propriétaire non trouvé' });
        }
        res.send(proprietaireModifie);
    } catch (error) {
        res.status(400).send({ message: error.message });
    }
});

router.delete('/:id', async (req, res) => {
    const { id } = req.params;
    try {
        const proprietaireSupprime = await Proprietaire.findByIdAndDelete(id);
        if (!proprietaireSupprime) {
            return res.status(404).send({ message: 'Propriétaire non trouvé' });
        }
        res.send(proprietaireSupprime);
    } catch (error) {
        res.status(500).send({ message: error.message });
    }
});

module.exports = router;
