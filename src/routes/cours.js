const mongoose = require('mongoose')
const express = require('express')
const router = express.Router()

const coursModel = require('../models/coursModel')

router.get('/', async (req, res) => {
    cours = await coursModel.find()
    res.send(cours)
})

module.exports = router