const mongoose = require('mongoose');

const { Schema, model } = mongoose;

const moniteurSchema = new Schema({
    adresse: String,
    civilite: String,
    dateNaissance: Date,
    ecoleId: Schema.Types.ObjectId,
    nom: String,
    prenom: String,
    localite: {
        localite: String,
        npa: String
    },
    mailMoniteurs: [{
        mail: String,
        type: String
    }],
    telephoneMoniteurs: [{
        numero: String,
        type: String
    }]
}, { collection: "moniteurs" });

module.exports = model("Moniteur", moniteurSchema);
