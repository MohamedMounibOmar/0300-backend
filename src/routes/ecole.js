const express = require('express');
const router = express.Router();
const Ecole = require('../models/ecoleModel');

router.get('/', async (req, res) => {
    try {
        const ecoles = await Ecole.find();
        res.send(ecoles);
    } catch (error) {
        res.status(500).send({ message: error.message });
    }
});

router.get('/:id', async (req, res) => {
    const { id } = req.params;
    try {
        const ecole = await Ecole.findById(id);
        if (!ecole) {
            return res.status(404).send({ message: 'École non trouvée' });
        }
        res.send(ecole);
    } catch (error) {
        res.status(500).send({ message: error.message });
    }
});

router.get('/aggregation', async (req, res) => {
    try {
        const aggregationResult = await Ecole.aggregate([
            {
                $lookup: {
                    from: "moniteurs",
                    localField: "_id",
                    foreignField: "ecoleId",
                    as: "moniteurs"
                }
            }
        ]);
        res.send(aggregationResult);
    } catch (error) {
        res.status(500).send({ message: error.message });
    }
});

router.post('/', async (req, res) => {
    try {
        const ecole = await Ecole.create(req.body);
        res.status(201).send(ecole);
    } catch (error) {
        res.status(400).send({ message: error.message });
    }
});

router.put('/:id', async (req, res) => {
    const { id } = req.params;
    try {
        const ecoleModifie = await Ecole.findByIdAndUpdate(id, req.body, { new: true });
        if (!ecoleModifie) {
            return res.status(404).send({ message: 'École non trouvée' });
        }
        res.send(ecoleModifie);
    } catch (error) {
        res.status(400).send({ message: error.message });
    }
});

router.delete('/:id', async (req, res) => {
    const { id } = req.params;
    try {
        const ecoleSupprime = await Ecole.findByIdAndDelete(id);
        if (!ecoleSupprime) {
            return res.status(404).send({ message: 'École non trouvée' });
        }
        res.send(ecoleSupprime);
    } catch (error) {
        res.status(500).send({ message: error.message });
    }
});

module.exports = router;
