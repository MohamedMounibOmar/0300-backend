const express = require('express');
const router = express.Router();
const Moniteur = require('../models/moniteurModel');

router.get('/', async (req, res) => {
    try {
        const moniteurs = await Moniteur.find();
        res.send(moniteurs);
    } catch (error) {
        res.status(500).send({ message: error.message });
    }
});

router.get('/:id', async (req, res) => {
    const { id } = req.params;
    try {
        const moniteur = await Moniteur.findById(id);
        if (!moniteur) {
            return res.status(404).send({ message: 'Moniteur non trouvé' });
        }
        res.send(moniteur);
    } catch (error) {
        res.status(500).send({ message: error.message });
    }
});

router.get('/aggregation', async (req, res) => {
    try {
        const aggregationResult = await Moniteur.aggregate([
            {
                $unwind: "$telephoneMoniteurs"
            }
        ]);
        res.send(aggregationResult);
    } catch (error) {
        res.status(500).send({ message: error.message });
    }
});

router.post('/', async (req, res) => {
    try {
        const moniteur = await Moniteur.create(req.body);
        res.status(201).send(moniteur);
    } catch (error) {
        res.status(400).send({ message: error.message });
    }
});

router.put('/:id', async (req, res) => {
    const { id } = req.params;
    try {
        const moniteurModifie = await Moniteur.findByIdAndUpdate(id, req.body, { new: true });
        if (!moniteurModifie) {
            return res.status(404).send({ message: 'Moniteur non trouvé' });
        }
        res.send(moniteurModifie);
    } catch (error) {
        res.status(400).send({ message: error.message });
    }
});

router.delete('/:id', async (req, res) => {
    const { id } = req.params;
    try {
        const moniteurSupprime = await Moniteur.findByIdAndDelete(id);
        if (!moniteurSupprime) {
            return res.status(404).send({ message: 'Moniteur non trouvé' });
        }
        res.send(moniteurSupprime);
    } catch (error) {
        res.status(500).send({ message: error.message });
    }
});

module.exports = router;
