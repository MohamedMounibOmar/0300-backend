const mongoose = require('mongoose');

const { Schema, model } = mongoose;

const ecoleSchema = new Schema({
    adresse: String,
    nom: String,
    localite: {
        localite: String,
        npa: Number
    },
    mailEcoles: [{
        mail: String,
        type: String
    }],
    telephoneEcoles: [{
        numero: String,
        type: String
    }]
}, { collection: 'ecoles' });

module.exports = model('Ecole', ecoleSchema);
