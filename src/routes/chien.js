const express = require('express');
const router = express.Router();
const Chien = require('../models/chienModel');

router.get('/', async (req, res) => {
    try {
        const chiens = await Chien.find();
        res.send(chiens);
    } catch (error) {
        res.status(500).send({ message: error.message });
    }
});

router.get('/:id', async (req, res) => {
    const { id } = req.params;
    try {
        const chien = await Chien.findById(id);
        if (!chien) {
            return res.status(404).send({ message: 'Chien non trouvé' });
        }
        res.send(chien);
    } catch (error) {
        res.status(500).send({ message: error.message });
    }
});

router.post('/', async (req, res) => {
    try {
        const chien = await Chien.create(req.body);
        res.status(201).send(chien);
    } catch (error) {
        res.status(400).send({ message: error.message });
    }
});

router.put('/:id', async (req, res) => {
    const { id } = req.params;
    try {
        const chienModifie = await Chien.findByIdAndUpdate(id, req.body, { new: true });
        if (!chienModifie) {
            return res.status(404).send({ message: 'Chien non trouvé' });
        }
        res.send(chienModifie);
    } catch (error) {
        res.status(400).send({ message: error.message });
    }
});

router.delete('/:id', async (req, res) => {
    const { id } = req.params;
    try {
        const chienSupprime = await Chien.findByIdAndDelete(id);
        if (!chienSupprime) {
            return res.status(404).send({ message: 'Chien non trouvé' });
        }
        res.send(chienSupprime);
    } catch (error) {
        res.status(500).send({ message: error.message });
    }
});

module.exports = router;
