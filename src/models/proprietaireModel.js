const mongoose = require('mongoose');

const { Schema, model } = mongoose;

const proprietaireSchema = new Schema({
    adresse: String,
    certificatDelivre: Date,
    dateNaissance: Date,
    nom: String,
    origine: String,
    prenom: String,
    localite: {
        localite: String,
        npa: Number
    },
    mailProprietaires: [{
        mail: String,
        type: String
    }],
    telephoneProprietaires: [{
        numero: String,
        type: String
    }]
}, { collection: "proprietaires" });

module.exports = model("Proprietaire", proprietaireSchema);
