const mongoose = require('mongoose');

const { Schema, model } = mongoose;

const chienSchema = new Schema({
    dateNaissance: Date,
    identificationAmicus: String,
    male: Boolean,
    nom: String,
    proprietaireId: Schema.Types.ObjectId,
    race: String
}, { collection: 'chiens' });

module.exports = model('Chien', chienSchema);
