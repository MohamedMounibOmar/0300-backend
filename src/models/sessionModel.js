const mongoose = require('mongoose');

const { Schema, model } = mongoose;

const sessionSchema = new Schema({
    date: Date,
    dureeM: Number,
    moniteurId: Schema.Types.ObjectId,
    places: Number,
    cour: {
        tarif: Number,
        niveau: String,
        type: String
    },
    participations: [{
        chienId: Schema.Types.ObjectId,
        avecProprietaire: Boolean
    }]
}, {
    collection: 'sessions'
});

module.exports = model('Session', sessionSchema);